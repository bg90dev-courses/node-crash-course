const EventEmitter = require("events");

// Create class
class MyEmitter extends EventEmitter {}

// Init object
const myEmmiter = new MyEmitter();

// Event Listener
myEmmiter.on("event", () => console.log("Event Fired!"));

// init event
myEmmiter.emit("event");
