# node-crash-course
![Course](https://img.shields.io/badge/Project-Course-yellow.svg)
![](https://img.shields.io/badge/Status-finished-purple.svg)

Curso intensivo de NodeJS de Traversy Media

https://www.youtube.com/watch?v=fBNz5xF-Kx4

## Features
- FileSystem module
- Operating System module
- Path module
- URL module
- Events module
- HTTP module

## Built With
<a href="https://nodejs.org/es/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/nodejs.png" width=50 alt="NodeJS"></a>

### Platforms
<a href="https://code.visualstudio.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vscode.png" width=50 alt="VSCode"></a>

## Author
* **Borja Gete** - <borjag90dev@gmail.com>